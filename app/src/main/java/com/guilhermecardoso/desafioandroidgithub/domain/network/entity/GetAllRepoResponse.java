package com.guilhermecardoso.desafioandroidgithub.domain.network.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by guilhermecardoso on 12/4/17.
 */

public class GetAllRepoResponse {

    @SerializedName("total_count")
    int totalCount;
    @SerializedName("incomplete_results")
    boolean imcompleteResults;
    @SerializedName("items")
    List<Repo> repos;

    public GetAllRepoResponse() {
    }

    public GetAllRepoResponse(int totalCount, boolean imcompleteResults, List<Repo> repos) {
        this.totalCount = totalCount;
        this.imcompleteResults = imcompleteResults;
        this.repos = repos;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public boolean isImcompleteResults() {
        return imcompleteResults;
    }

    public void setImcompleteResults(boolean imcompleteResults) {
        this.imcompleteResults = imcompleteResults;
    }

    public List<Repo> getRepos() {
        return repos;
    }

    public void setRepos(List<Repo> repos) {
        this.repos = repos;
    }
}
