package com.guilhermecardoso.desafioandroidgithub.domain.network.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by guilhermecardoso on 11/27/17.
 */

public class Repo implements Parcelable{

    @SerializedName("name")
    private String name;
    @SerializedName("language")
    private String language;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("description")
    private String description;
    @SerializedName("forks_count")
    private Long forkCount;
    @SerializedName("stargazers_count")
    private long starCount;
    @SerializedName("owner")
    private User owner;

    public Repo() {
    }

    public Repo(String name, String language, String fullName, String description, Long forkCount, long starCount, User owner) {
        this.name = name;
        this.language = language;
        this.fullName = fullName;
        this.description = description;
        this.forkCount = forkCount;
        this.starCount = starCount;
        this.owner = owner;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getForkCount() {
        return forkCount;
    }

    public void setForkCount(Long forkCount) {
        this.forkCount = forkCount;
    }

    public long getStarCount() {
        return starCount;
    }

    public void setStarCount(long starCount) {
        this.starCount = starCount;
    }

    protected Repo(Parcel in) {
        this.name = (in.readString());
        this.language = (in.readString());
        this.fullName = in.readString();
        this.description = in.readString();
        this.forkCount = in.readLong();
        this.starCount = in.readLong();
        this.owner = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<Repo> CREATOR = new Creator<Repo>() {
        @Override
        public Repo createFromParcel(Parcel in) {
            return new Repo(in);
        }

        @Override
        public Repo[] newArray(int size) {
            return new Repo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeString(this.language);
        parcel.writeString(this.fullName);
        parcel.writeString(this.description);
        parcel.writeLong(this.forkCount);
        parcel.writeLong(this.starCount);
        parcel.writeParcelable(this.owner, i);
    }

    public String getForkCountFormatted() {
        return this.forkCount + " forks";
    }

    public String getStarCountFormatted() {
        return this.starCount + " stars";
    }
}
