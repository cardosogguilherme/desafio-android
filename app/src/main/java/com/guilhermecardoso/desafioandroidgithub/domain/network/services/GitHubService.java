package com.guilhermecardoso.desafioandroidgithub.domain.network.services;

import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.GetAllRepoResponse;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.PullRequest;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.Repo;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.User;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by guilhermecardoso on 11/27/17.
 */

public interface GitHubService {
    String SERVICE_ENDPOINT = "https://api.github.com/";
    String SERVICE_TOKEN = null;

    @GET("search/repositories")
    Single<GetAllRepoResponse> getReposByLanguage(@Query(value = "q", encoded = true) String query);
    @GET("repos/{user}/{repo}/pulls")
    Single<List<PullRequest>> getPullRequestsFromRepo(@Path("user") String user, @Path("repo") String repo);

    @GET("{username}/repos")
    Single<List<Repo>> getUserRepos(@Path("username") String username);
    @GET("{username}")
    Single<User> getUser(@Path("username") String username);
}
