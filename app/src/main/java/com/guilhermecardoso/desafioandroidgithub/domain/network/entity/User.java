package com.guilhermecardoso.desafioandroidgithub.domain.network.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by guilhermecardoso on 11/28/17.
 */

public class User implements Parcelable {

    @SerializedName("login")
    private String name;
    @SerializedName("avatar_url")
    private String avatarURL;

    public User() {
    }

    public User(String name, String avatarURL) {
        this.name = name;
        this.avatarURL = avatarURL;
    }

    public User(Parcel in) {
        this.name = in.readString();
        this.avatarURL = in.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }

    public String getFullName() {
        return "Mock";
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeString(this.avatarURL);
    }
}
