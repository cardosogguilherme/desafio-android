package com.guilhermecardoso.desafioandroidgithub.domain.network;

/**
 * Created by guilhermecardoso on 12/4/17.
 */

public class QueryBuilder {

    public static String buildQueryGitHubWithLanguage(String language, String sort, int page) {
        String query = "language=" + language + "&sort=" + sort + "&page=" + page;
        return query;
//        try {
//            return URLEncoder.encode(query, "utf-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//            return "";
//        }
    }
}
