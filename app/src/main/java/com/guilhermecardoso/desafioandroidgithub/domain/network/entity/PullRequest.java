package com.guilhermecardoso.desafioandroidgithub.domain.network.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by guilhermecardoso on 12/5/17.
 */

public class PullRequest {
    @SerializedName("html_url")
    private String htmlURL;
    @SerializedName("title")
    private String title;
    @SerializedName("user")
    private User user;
    @SerializedName("body")
    private String body;
    @SerializedName("created_at")
    private String createdAt;

    public PullRequest() {
    }

    public PullRequest(String htmlURL, String title, User user, String body, String createdAt) {
        this.htmlURL = htmlURL;
        this.title = title;
        this.user = user;
        this.body = body;
        this.createdAt = createdAt;
    }

    public String getHtmlURL() {
        return htmlURL;
    }

    public void setHtmlURL(String htmlURL) {
        this.htmlURL = htmlURL;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getFormattedBody() {
        return this.body.isEmpty() ? "No description" : this.body;
    }
}
