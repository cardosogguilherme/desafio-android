package com.guilhermecardoso.desafioandroidgithub.domain.network.interceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by guilhermecardoso on 12/4/17.
 */

public class FormattingInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        String url = request.url().toString();
        url = url.replace("%26", "&").replace("%3D", "=");

        Request newRequest = new Request.Builder()
                .url(url)
                .build();

        return chain.proceed(newRequest);
    }
}
