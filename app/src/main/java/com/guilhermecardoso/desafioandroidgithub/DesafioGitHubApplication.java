package com.guilhermecardoso.desafioandroidgithub;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by guilhermecardoso on 12/4/17.
 */

public class DesafioGitHubApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }
}
