package com.guilhermecardoso.desafioandroidgithub.app.modules.repopulls;

import com.guilhermecardoso.desafioandroidgithub.app.modules.BaseActions;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.PullRequest;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.Repo;

import java.util.List;

/**
 * Created by guilhermecardoso on 12/5/17.
 */

public interface RepoPullsContract {

    interface View {
        void showNoPullsMessage();

        void showLoading();
        void hideLoading();
        void showError();
        void setPresenter(Actions presenter);
        void showPulls(List<PullRequest> pullRequests);
        void navigateToPullDetails(PullRequest pullRequest);
    }

    interface Actions extends BaseActions {
        void loadRepoDetails(Repo repo);
        void attachView(View view);
        void openPullDetails(PullRequest pullRequest);
    }
}
