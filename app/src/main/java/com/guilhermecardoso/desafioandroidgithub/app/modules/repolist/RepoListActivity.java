package com.guilhermecardoso.desafioandroidgithub.app.modules.repolist;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.guilhermecardoso.desafioandroidgithub.R;
import com.guilhermecardoso.desafioandroidgithub.domain.network.ServiceFactory;
import com.guilhermecardoso.desafioandroidgithub.domain.network.services.GitHubService;

import io.reactivex.disposables.CompositeDisposable;

public class RepoListActivity extends AppCompatActivity {

    private static final String TAG = "RepoListActivity";
    private RepoListContract.Actions presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_list);
        setupFragment();
    }

    private void setupFragment() {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = fragmentManager.findFragmentByTag(TAG);

        if (fragment == null) {
            fragment = RepoListFragment.newInstance();
        }

        presenter = (RepoListContract.Actions) getLastCustomNonConfigurationInstance();
        if (presenter == null) {
            presenter = new RepoListPresenter((RepoListFragment) fragment,
                    new CompositeDisposable(),
                    ServiceFactory.createService(GitHubService.class));
        } else {
            presenter.attachView((RepoListFragment) fragment);
        }

        ((RepoListFragment) fragment).setPresenter(presenter);
        fragmentTransaction.replace(R.id.fragment, fragment, TAG);
        fragmentTransaction.commit();
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return presenter;
    }
}
