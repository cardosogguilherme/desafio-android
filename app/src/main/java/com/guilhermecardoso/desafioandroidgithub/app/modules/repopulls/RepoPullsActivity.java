package com.guilhermecardoso.desafioandroidgithub.app.modules.repopulls;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.guilhermecardoso.desafioandroidgithub.R;
import com.guilhermecardoso.desafioandroidgithub.domain.network.ServiceFactory;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.Repo;
import com.guilhermecardoso.desafioandroidgithub.domain.network.services.GitHubService;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by guilhermecardoso on 12/5/17.
 */

public class RepoPullsActivity extends AppCompatActivity {

    private static final String TAG = "RepoPullsActivity";
    public static final String EXTRA_REPO = "EXTRA_REPO";
    private RepoPullsContract.Actions presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_list);

        Repo repo = null;

        Intent intent = getIntent();
        if (intent != null) {
            repo = intent.getParcelableExtra(EXTRA_REPO);

            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setTitle(repo.getFullName());
            }
        }

        setupFragment(repo);
    }

    private void setupFragment(Repo repo) {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Fragment fragment = fragmentManager.findFragmentByTag(TAG);


        if (fragment == null) {
            fragment = RepoPullsFragment.newInstance(repo);
        }

        presenter = (RepoPullsContract.Actions) getLastCustomNonConfigurationInstance();
        if (presenter == null) {
            presenter = new RepoPullsPresenter((RepoPullsFragment) fragment,
                    new CompositeDisposable(),
                    ServiceFactory.createService(GitHubService.class));
        } else {
            presenter.attachView((RepoPullsFragment) fragment);
        }

        ((RepoPullsFragment) fragment).setPresenter(presenter);
        fragmentTransaction.replace(R.id.fragment, fragment, TAG);
        fragmentTransaction.commit();
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return presenter;
    }
}
