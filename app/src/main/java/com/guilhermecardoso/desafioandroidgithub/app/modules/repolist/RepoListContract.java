package com.guilhermecardoso.desafioandroidgithub.app.modules.repolist;

import com.guilhermecardoso.desafioandroidgithub.app.modules.BaseActions;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.Repo;

import java.util.List;

/**
 * Created by guilhermecardoso on 12/4/17.
 */

public interface RepoListContract {

    interface View {
        void showLoading();
        void hideLoading();
        void showError();
        void showRepos(List<Repo> repos);
        void setPresenter(Actions presenter);
        void navigateToRepoPullScreen(Repo repo);
    }

    interface Actions extends BaseActions {
        void loadRepos(int page);
        void attachView(View view);
        void loadRepoDetails(Repo repo);
    }
}
