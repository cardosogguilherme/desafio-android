package com.guilhermecardoso.desafioandroidgithub.app.modules.repolist.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.guilhermecardoso.desafioandroidgithub.R;
import com.guilhermecardoso.desafioandroidgithub.app.modules.repolist.RepoListContract;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.Repo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guilhermecardoso on 12/4/17.
 */

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.ViewHolder> {

    List<Repo> repos;
    RepoListContract.Actions presenter;

    public RepoAdapter(RepoListContract.Actions presenter) {
        repos = new ArrayList<>();
        this.presenter = presenter;
    }

    public void addRepos(List<Repo> repos) {
        this.repos.addAll(repos);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_repo, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Repo repo = this.repos.get(position);

        holder.textviewRepoName.setText(repo.getName());
        holder.textviewRepoDesc.setText(repo.getDescription());
        holder.textviewRepoForkCount.setText(repo.getForkCountFormatted());
        holder.textviewRepoStarCount.setText(repo.getStarCountFormatted());
        holder.textviewUsername.setText(repo.getOwner().getName());

        Uri uri = Uri.parse(repo.getOwner().getAvatarURL());
        holder.draweeUserImage.setImageURI(uri);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.loadRepoDetails(repo);
            }
        });

    }

    @Override
    public int getItemCount() {
        return repos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.text_repo_name)
        TextView textviewRepoName;
        @BindView(R.id.text_repo_desc)
        TextView textviewRepoDesc;
        @BindView(R.id.text_fork_count)
        TextView textviewRepoForkCount;
        @BindView(R.id.text_star_count)
        TextView textviewRepoStarCount;
        @BindView(R.id.text_username)
        TextView textviewUsername;
        @BindView(R.id.drawee_user_image)
        SimpleDraweeView draweeUserImage;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
