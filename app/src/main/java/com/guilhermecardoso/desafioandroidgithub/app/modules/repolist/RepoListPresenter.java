package com.guilhermecardoso.desafioandroidgithub.app.modules.repolist;

import com.guilhermecardoso.desafioandroidgithub.domain.network.QueryBuilder;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.GetAllRepoResponse;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.Repo;
import com.guilhermecardoso.desafioandroidgithub.domain.network.services.GitHubService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by guilhermecardoso on 12/4/17.
 */

public class RepoListPresenter implements RepoListContract.Actions{


    private static final String TAG = "RepoListPresenter";
    private RepoListContract.View view;
    private CompositeDisposable compositeDisposable;
    private final GitHubService service;

    public RepoListPresenter(RepoListContract.View view, CompositeDisposable compositeDisposable, GitHubService service) {
        this.view = view;
        this.compositeDisposable = compositeDisposable;
        this.service = service;
    }

    @Override
    public void dispose() {
        if (this.compositeDisposable != null && !this.compositeDisposable.isDisposed()) {
            this.compositeDisposable.dispose();
        }
    }

    @Override
    public void subscribe() {
        this.compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void loadRepos(int page) {
        view.showLoading();
        this.compositeDisposable.add(
                service.getReposByLanguage(QueryBuilder.buildQueryGitHubWithLanguage("Java", "stars", page))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new BiConsumer<GetAllRepoResponse, Throwable>() {
                    @Override
                    public void accept(GetAllRepoResponse getAllRepoResponse, Throwable throwable) throws Exception {
                        view.hideLoading();
                        if (getAllRepoResponse != null) {
                            view.showRepos(getAllRepoResponse.getRepos());
                        } else {
                            throwable.printStackTrace();
                            view.showError();
                        }
                    }
                })
        );
    }

    @Override
    public void loadRepoDetails(Repo repo) {
        view.navigateToRepoPullScreen(repo);
    }

    @Override
    public void attachView(RepoListContract.View view) {
        this.view = view;
    }
}
