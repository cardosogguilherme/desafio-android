package com.guilhermecardoso.desafioandroidgithub.app.modules.repolist;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.guilhermecardoso.desafioandroidgithub.R;
import com.guilhermecardoso.desafioandroidgithub.app.modules.repolist.adapter.RepoAdapter;
import com.guilhermecardoso.desafioandroidgithub.app.modules.repopulls.RepoPullsActivity;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.Repo;
import com.guilhermecardoso.desafioandroidgithub.lib.listener.EndlessScrollListener;

import java.util.List;

import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by guilhermecardoso on 12/4/17.
 */

public class RepoListFragment extends Fragment implements RepoListContract.View {

    private RepoListContract.Actions presenter;
    private SweetAlertDialog dialog;
    private static String TAG = "RepoListFragment";
    private RepoAdapter adapter;


    public static Fragment newInstance() {
        RepoListFragment instance = new RepoListFragment();
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.fragment_list_repos, container, false);
        ButterKnife.bind(this, rootView);

        RecyclerView recyclerView = ButterKnife.findById(rootView, R.id.recycler_view);
        adapter = new RepoAdapter(this.presenter);

        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(false);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.addOnScrollListener(new EndlessScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                presenter.loadRepos(page);
            }
        });

        presenter.loadRepos(1);

        return rootView;
    }


    @Override
    public void setPresenter(RepoListContract.Actions presenter) {
        this.presenter = presenter;
        this.presenter.subscribe();
    }

    @Override
    public void showLoading() {
        FragmentActivity activity = getActivity();
        if (dialog == null && activity != null) {
            dialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE);
            dialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            dialog.setTitleText("Loading");
            dialog.setCancelable(true);
            dialog.show();
        }
    }

    @Override
    public void hideLoading() {
        if (dialog != null) {
            dialog.dismissWithAnimation();
            dialog = null;
        }
    }

    @Override
    public void showError() {
        FragmentActivity activity = getActivity();
        if (dialog == null && activity != null) {
            dialog = new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE);
            dialog.setTitleText("Some error occurred");
            dialog.setContentText("A network error has occurred. Check your Internet connection and try again later");
            dialog.setCancelable(true);
            dialog.show();
        }
    }

    @Override
    public void showRepos(List<Repo> repos) {
        adapter.addRepos(repos);
    }

    @Override
    public void onDestroyView() {
        presenter.dispose();
        super.onDestroyView();
    }

    @Override
    public void navigateToRepoPullScreen(Repo repo) {
        Intent intent = new Intent(getActivity(), RepoPullsActivity.class);

        intent.putExtra(RepoPullsActivity.EXTRA_REPO, repo);

        startActivity(intent);
    }
}
