package com.guilhermecardoso.desafioandroidgithub.app.modules.repopulls;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.guilhermecardoso.desafioandroidgithub.R;
import com.guilhermecardoso.desafioandroidgithub.app.modules.repopulls.adapter.RepoPullsAdapter;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.PullRequest;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.Repo;

import java.util.List;

import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.guilhermecardoso.desafioandroidgithub.app.modules.repopulls.RepoPullsActivity.EXTRA_REPO;

/**
 * Created by guilhermecardoso on 12/5/17.
 */

public class RepoPullsFragment extends Fragment implements RepoPullsContract.View{

    private SweetAlertDialog dialog;
    private RepoPullsContract.Actions presenter;
    private RepoPullsAdapter adapter;


    public static RepoPullsFragment newInstance(Repo repo) {
        RepoPullsFragment instance = new RepoPullsFragment();

        Bundle args = new Bundle();
        args.putParcelable(EXTRA_REPO, repo);

        instance.setArguments(args);

        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View rootView = inflater.inflate(R.layout.fragment_repo_pulls, container, false);
        ButterKnife.bind(this, rootView);

        RecyclerView recyclerView = ButterKnife.findById(rootView, R.id.recycler_view);
        adapter = new RepoPullsAdapter(this.presenter);

        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        Bundle args = getArguments();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setAutoMeasureEnabled(false);
        recyclerView.setLayoutManager(linearLayoutManager);

        if (args.containsKey(EXTRA_REPO)) {
            Repo repo = args.getParcelable(EXTRA_REPO);
            presenter.loadRepoDetails(repo);
        } else {
            showNoPullsMessage();
        }

        return rootView;
    }

    @Override
    public void navigateToPullDetails(PullRequest pullRequest) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(pullRequest.getHtmlURL()));
        startActivity(intent);
    }

    @Override
    public void showNoPullsMessage() {
        FragmentActivity activity = getActivity();
        if (dialog == null && activity != null) {
            dialog = new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE);
            dialog.setTitleText("No PR from this repository");
            dialog.setCancelable(true);
            dialog.show();
        }
    }


    @Override
    public void showLoading() {
        FragmentActivity activity = getActivity();
        if (dialog == null && activity != null) {
            dialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE);
            dialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            dialog.setTitleText("Loading");
            dialog.setCancelable(true);
            dialog.show();
        }
    }

    @Override
    public void hideLoading() {
        if (dialog != null) {
            dialog.dismissWithAnimation();
            dialog = null;
        }
    }

    @Override
    public void showError() {
        FragmentActivity activity = getActivity();
        if (dialog == null && activity != null) {
            dialog = new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE);
            dialog.setTitleText("Some error occurred");
            dialog.setContentText("A network error has occurred. Check your Internet connection and try again later");
            dialog.setCancelable(true);
            dialog.show();
        }
    }

    @Override
    public void showPulls(List<PullRequest> pullRequests) {
        this.adapter.addPullRequests(pullRequests);
    }

    @Override
    public void onDestroyView() {
        presenter.dispose();
        super.onDestroyView();
    }

    @Override
    public void setPresenter(RepoPullsContract.Actions presenter) {
        this.presenter = presenter;
        this.presenter.subscribe();
    }
}
