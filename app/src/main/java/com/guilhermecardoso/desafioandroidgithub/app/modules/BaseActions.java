package com.guilhermecardoso.desafioandroidgithub.app.modules;

/**
 * Created by guilhermecardoso on 11/27/17.
 */

public interface BaseActions {
    void dispose();
    void subscribe();
}
