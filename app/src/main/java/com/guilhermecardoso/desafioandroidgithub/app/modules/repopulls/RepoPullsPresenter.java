package com.guilhermecardoso.desafioandroidgithub.app.modules.repopulls;

import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.PullRequest;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.Repo;
import com.guilhermecardoso.desafioandroidgithub.domain.network.services.GitHubService;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by guilhermecardoso on 12/5/17.
 */

public class RepoPullsPresenter implements RepoPullsContract.Actions {

    private static final String TAG = "RepoPullsPresenter";
    private RepoPullsContract.View view;
    private CompositeDisposable compositeDisposable;
    private GitHubService service;

    public RepoPullsPresenter(RepoPullsContract.View view, CompositeDisposable compositeDisposable, GitHubService service) {
        this.view = view;
        this.compositeDisposable = compositeDisposable;
        this.service = service;
    }

    @Override
    public void dispose() {
        if (this.compositeDisposable != null && !this.compositeDisposable.isDisposed()) {
            this.compositeDisposable.dispose();
        }
    }

    @Override
    public void subscribe() {
        this.compositeDisposable = new CompositeDisposable();
    }


    @Override
    public void loadRepoDetails(final Repo repo) {
        view.showLoading();
        this.compositeDisposable.add(service.getPullRequestsFromRepo(repo.getOwner().getName(), repo.getName())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.newThread())
        .subscribe(new BiConsumer<List<PullRequest>, Throwable>() {
            @Override
            public void accept(List<PullRequest> pullRequests, Throwable throwable) throws Exception {
                view.hideLoading();
                if (pullRequests != null) {
                    if (pullRequests.size() > 0) {
                        view.showPulls(pullRequests);
                    } else {
                        view.showNoPullsMessage();
                    }
                } else {
                    throwable.printStackTrace();
                    view.showError();
                }
            }
        }));
    }

    @Override
    public void openPullDetails(PullRequest pullRequest) {
        view.navigateToPullDetails(pullRequest);
    }

    @Override
    public void attachView(RepoPullsContract.View view) {
        this.view = view;
    }
}
