package com.guilhermecardoso.desafioandroidgithub.app.modules.repopulls.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.guilhermecardoso.desafioandroidgithub.R;
import com.guilhermecardoso.desafioandroidgithub.app.modules.repopulls.RepoPullsContract;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.PullRequest;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by guilhermecardoso on 12/5/17.
 */

public class RepoPullsAdapter extends RecyclerView.Adapter<RepoPullsAdapter.ViewHolder> {

    List<PullRequest> pullRequests;
    RepoPullsContract.Actions presenter;

    public RepoPullsAdapter(RepoPullsContract.Actions presenter) {
        pullRequests = new ArrayList<>();
        this.presenter = presenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_pull_request, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PullRequest pullRequest = this.pullRequests.get(position);

        holder.textTitle.setText(pullRequest.getTitle());
        holder.textDesc.setText(pullRequest.getFormattedBody());
        holder.textUserName.setText(pullRequest.getUser().getName());

        Uri uri = Uri.parse(pullRequest.getUser().getAvatarURL());
        holder.draweeUserImage.setImageURI(uri);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.openPullDetails(pullRequest);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    public void addPullRequests(List<PullRequest> pullRequests) {
        this.pullRequests.addAll(pullRequests);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.drawee_user_url)
        SimpleDraweeView draweeUserImage;
        @BindView(R.id.text_username)
        TextView textUserName;
        @BindView(R.id.text_title)
        TextView textTitle;
        @BindView(R.id.text_desc)
        TextView textDesc;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
