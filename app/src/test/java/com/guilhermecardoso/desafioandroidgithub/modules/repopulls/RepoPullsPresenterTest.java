package com.guilhermecardoso.desafioandroidgithub.modules.repopulls;

import com.guilhermecardoso.desafioandroidgithub.app.modules.repopulls.RepoPullsContract;
import com.guilhermecardoso.desafioandroidgithub.app.modules.repopulls.RepoPullsPresenter;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.PullRequest;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.Repo;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.User;
import com.guilhermecardoso.desafioandroidgithub.domain.network.services.GitHubService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by guilhermecardoso on 12/6/17.
 */

public class RepoPullsPresenterTest {

    private RepoPullsContract.Actions presenter;
    @Mock
    private RepoPullsContract.View mockedView;
    @Mock
    GitHubService mockedService;
    @Mock
    Repo mockedRepo;
    @Mock
    User mockedUser;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new RepoPullsPresenter(mockedView, new CompositeDisposable(), mockedService);
    }

    @Before
    public void setupClass() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(
                new Function<Callable<Scheduler>, Scheduler>() {
                    @Override
                    public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                        return Schedulers.newThread();
                    }
                }
        );
        RxJavaPlugins.setNewThreadSchedulerHandler(new Function<Scheduler, Scheduler>() {
            @Override
            public Scheduler apply(Scheduler scheduler) throws Exception {
                return Schedulers.trampoline();
            }
        });
    }

    @After
    public void cleanUp() {
        Schedulers.shutdown();
    }

    private void mockRepoSetup() {
        when(mockedRepo.getName()).thenReturn("mockedRepo");
        when(mockedRepo.getOwner()).thenReturn(mockedUser);
        when(mockedUser.getName()).thenReturn("mockedUser");
    }

    @Test
    public void shouldLoadRepoList() {
        //Given some pullrequests
        mockServiceResponseWithItems();
        mockRepoSetup();
        //When we try to load
        presenter.loadRepoDetails(mockedRepo);
        //The view should receive the list to show
        verify(mockedView).showPulls(any(List.class));
    }

    @Test
    public void shouldShowEmptyList() {
        //Given no pulls
        mockServiceResponseWithoutItems();
        mockRepoSetup();
        //When we try to load
        presenter.loadRepoDetails(mockedRepo);
        //The view should still receive the list to show
        verify(mockedView).showNoPullsMessage();
    }

    @Test
    public void shouldShowError() {
        //Given an error
        mockServiceError();
        mockRepoSetup();
        //When we try to load
        presenter.loadRepoDetails(mockedRepo);
        //The view should still receive the list to show
        verify(mockedView).showError();
    }

    private void mockServiceError() {
        when(mockedService.getPullRequestsFromRepo(anyString(), anyString()))
                .thenReturn(Single.<List<PullRequest>>error(new NullPointerException()));
    }

    private void mockServiceResponseWithItems() {
        List<PullRequest> response = mockResponseWithItems();

        when(mockedService.getPullRequestsFromRepo(anyString(), anyString()))
                .thenReturn(Single.just(response));
    }

    private void mockServiceResponseWithoutItems() {
        List<PullRequest> response = mockResponseWithoutItems();

        when(mockedService.getPullRequestsFromRepo(anyString(), anyString()))
                .thenReturn(Single.just(response));
    }

    private List<PullRequest> mockResponseWithoutItems() {
        return new ArrayList<PullRequest>();
    }

    private List<PullRequest> mockResponseWithItems() {
        return Arrays.asList(new PullRequest());
    }
}
