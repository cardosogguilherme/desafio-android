package com.guilhermecardoso.desafioandroidgithub.modules.repolist;

import com.guilhermecardoso.desafioandroidgithub.app.modules.repolist.RepoListContract;
import com.guilhermecardoso.desafioandroidgithub.app.modules.repolist.RepoListPresenter;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.GetAllRepoResponse;
import com.guilhermecardoso.desafioandroidgithub.domain.network.entity.Repo;
import com.guilhermecardoso.desafioandroidgithub.domain.network.services.GitHubService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by guilhermecardoso on 12/6/17.
 */

public class RepoListPresenterTest {

    private RepoListContract.Actions presenter;
    @Mock
    private RepoListContract.View mockedView;
    @Mock
    GitHubService mockedService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new RepoListPresenter(mockedView, new CompositeDisposable(), mockedService);
    }

    @Before
    public void setupClass() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(
                new Function<Callable<Scheduler>, Scheduler>() {
                    @Override
                    public Scheduler apply(Callable<Scheduler> schedulerCallable) throws Exception {
                        return Schedulers.newThread();
                    }
                }
        );
        RxJavaPlugins.setNewThreadSchedulerHandler(new Function<Scheduler, Scheduler>() {
            @Override
            public Scheduler apply(Scheduler scheduler) throws Exception {
                return Schedulers.trampoline();
            }
        });
    }

    @After
    public void cleanUp() {
        Schedulers.shutdown();
    }

    @Test
    public void shouldLoadRepoList() {
        //Given a page and repos
        int page = 1;
        mockServiceResponseWithItems();
        //When we try to load
        presenter.loadRepos(page);
        //The view should receive the list to show
        verify(mockedView).showRepos(any(List.class));
    }

    @Test
    public void shouldShowEmptyList() {
        //Given a page and no repos
        int page = 1;
        mockServiceResponseWithoutItems();
        //When we try to load
        presenter.loadRepos(page);
        //The view should still receive the list to show
        verify(mockedView).showRepos(any(List.class));
    }

    @Test
    public void shouldShowError() {
        //Given a page and an error
        int page = 1;
        mockServiceError();
        //When we try to load
        presenter.loadRepos(page);
        //The view should still receive the list to show
        verify(mockedView).showError();
    }

    private void mockServiceError() {
        when(mockedService.getReposByLanguage(anyString()))
                .thenReturn(Single.<GetAllRepoResponse>error(new NullPointerException()));
    }

    private void mockServiceResponseWithItems() {
        GetAllRepoResponse response = mockResponseWithItems();

        when(mockedService.getReposByLanguage(anyString()))
                .thenReturn(Single.just(response));
    }

    private void mockServiceResponseWithoutItems() {
        GetAllRepoResponse response = mockResponseWithoutItems();

        when(mockedService.getReposByLanguage(anyString()))
                .thenReturn(Single.just(response));
    }

    private GetAllRepoResponse mockResponseWithoutItems() {
        return new GetAllRepoResponse(0, false, new ArrayList<Repo>());
    }

    private GetAllRepoResponse mockResponseWithItems() {
        return new GetAllRepoResponse(1, false, Arrays.asList(new Repo()));
    }
}
